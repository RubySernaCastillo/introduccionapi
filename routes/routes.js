const { response } = require('express');
const { request } = require('express');
const pool = require('../data/config');

const router = function(app) {
    app.get('/', function(request,response){
        response.send({
            message: 'Bienvenido a node.js express REST API!'
        });
    });

 app.get('/users', function (request, response) {
     pool.query('SELECT * FROM users', function (error, result) {
         if(error)
         throw error;

         response.send(result);
     });
 })

app.get('/users/:id',function(request, response){
    const {id} = request.params;

    pool.query('SELECT * FROM users WHERE id = ?', id, function(error, result){
        if (error)
        throw error;
        response.send(result);
    } );
});

app.post('/users',(request, response) => {
    pool.query('INSERT INTO user SET ?', request.body, (error, result) => {
        if (error) 
        throw error
        response.status(201).send(`User added with ID: ${result.insertID}`);
    });
});

app.put('/users/:id',(request,response)=> {
    const id= request.params.id

    pool.query('UPDATE users set ? WHERE id = ?', [request.body,id], (error, result)=>{
        if (error)
        throw error;

        response.send('User updated succesfully. ');

    });
});

app.delete('/users/:id',  (request, response)=> {
    const id= request.params.id;

    pool.query('DELETE FROM users WHERE id = ? ',id, (error, result)=>{
        if (error)
        throw error;

        response.send('User deleted. ');
    });
});

}

module.exports=router;